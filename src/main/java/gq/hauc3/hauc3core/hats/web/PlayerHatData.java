package gq.hauc3.hauc3core.hats.web;

@SuppressWarnings("ClassCanBeRecord")
public class PlayerHatData {
    private final String hat;

    public PlayerHatData(String hat) {
        this.hat = hat;
    }

    public String getHatType() {
        return hat;
    }
}
