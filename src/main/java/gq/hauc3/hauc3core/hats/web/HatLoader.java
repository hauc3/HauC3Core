package gq.hauc3.hauc3core.hats.web;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import gq.hauc3.hauc3core.Main;
import net.minecraft.client.MinecraftClient;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@SuppressWarnings("UnstableApiUsage")
public class HatLoader {
    private static final Logger logger = Main.logger;
    private final static String HATS_URL = "https://hauc3.gq/api/hats";
    public static final Type HAT_TYPE = new TypeToken<Map<UUID, PlayerHatData>>(){}.getType();
    public static Map<UUID, PlayerHatData> PLAYER_HATS;
    private static final Gson GSON = new GsonBuilder().create();

    public static void init() {
        logger.info("Initializing hats...");
        CompletableFuture.supplyAsync(() -> {
            try (Reader reader = new InputStreamReader(new URL(HATS_URL).openStream())) {
                var out = GSON.<Map<UUID, PlayerHatData>>fromJson(reader, HAT_TYPE);
                logger.info("Hats:");
                out.forEach((uuid, playerHatData) -> logger.info(uuid + " ~ " + playerHatData.getHatType()));
                return out;
            } catch (MalformedURLException error) {
                logger.error("Unable to load player hats because of connection problems: " + error.getMessage());
            } catch (IOException error) {
                logger.error("Unable to load player hats because of an I/O Exception: " + error.getMessage());
            }

            return null;
        }).thenAcceptAsync(playerData -> {
            if (playerData != null) {
                PLAYER_HATS = playerData;
                logger.info("Player hats successfully loaded!");
            } else {
                PLAYER_HATS = Collections.emptyMap();
                logger.warn("A problem with the database occurred, the hats could not be initialized.");
            }
        }, MinecraftClient.getInstance());
    }
}
