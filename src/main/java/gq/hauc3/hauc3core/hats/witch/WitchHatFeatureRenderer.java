package gq.hauc3.hauc3core.hats.witch;

import gq.hauc3.hauc3core.hats.web.HatLoader;
import gq.hauc3.hauc3core.util.ColorUtil;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.model.TexturedModelData;
import net.minecraft.client.render.*;
import net.minecraft.client.render.entity.feature.FeatureRenderer;
import net.minecraft.client.render.entity.feature.FeatureRendererContext;
import net.minecraft.client.render.entity.model.EntityModel;
import net.minecraft.client.render.entity.model.EntityModelLayer;
import net.minecraft.client.render.entity.model.EntityModelLoader;
import net.minecraft.client.render.entity.model.ModelWithHead;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;

import java.awt.*;
import java.util.UUID;

import static gq.hauc3.hauc3core.Main.modID;

@Environment(EnvType.CLIENT)
public class WitchHatFeatureRenderer<T extends LivingEntity, M extends EntityModel<T>> extends FeatureRenderer<T, M> {
    public static final EntityModelLayer WITCH_HAT_MODEL_LAYER = new EntityModelLayer(new Identifier("hauc3core", "witch_hat"), "main");
    private static final UUID NEDMAC = UUID.fromString("e594cbdb-23e5-44ad-8020-539b17d4da57");

    private static final Identifier WITCH = new Identifier("textures/entity/witch.png");
    private static final Identifier OVERLAY = new Identifier(modID, "textures/hats/overlay.png");
    private static final Color OWNER_COLOR = ColorUtil.radialRainbow(1,1);
    private static final Color MANAGER_COLOR = ColorUtil.hex2Rgb("a84300");
    private static final Color DEVELOPER_COLOR = ColorUtil.hex2Rgb("00e2ff");
    private static final Color ADMIN_COLOR = ColorUtil.hex2Rgb("e74c3c");
    private static final Color MOD_COLOR = ColorUtil.hex2Rgb("e67e22");
    private static final Color TRIAL_MOD_COLOR = ColorUtil.hex2Rgb("f1c40f");
    private static final Color TRAINEE_COLOR = ColorUtil.hex2Rgb("9bff00");
    private static final Color FORMER_STAFF_COLOR = ColorUtil.hex2Rgb("9d3dd3");
    private static final Color DISABLED_COLOR = ColorUtil.hex2Rgb("ab9f0f");
    private final WitchHatModel<T> witchHat;
    private final MinecraftClient client = MinecraftClient.getInstance();

    public WitchHatFeatureRenderer(FeatureRendererContext<T, M> featureRendererContext, EntityModelLoader entityModelLoader) {
        super(featureRendererContext);
        this.witchHat = new WitchHatModel<>(entityModelLoader.getModelPart(WITCH_HAT_MODEL_LAYER));
    }

    public static TexturedModelData getTexturedModelData() {
        return TexturedModelData.of(WitchHatModel.getModelData(), 64, 128);
    }

    public void render(MatrixStack matrixStack, VertexConsumerProvider vertexConsumerProvider, int i, T livingEntity, float f, float g, float h, float j, float k, float l) {
        Color hat_type = getHat(livingEntity.getUuid());

        if (hat_type != null && !livingEntity.isInvisibleTo(client.player) && hat_type != DISABLED_COLOR) {
            if (hat_type == OWNER_COLOR) hat_type = ColorUtil.radialRainbow(1,1);
            matrixStack.push();

            ((ModelWithHead) this.getContextModel()).getHead().rotate(matrixStack);
            VertexConsumer vertexConsumer = vertexConsumerProvider.getBuffer(RenderLayer.getEntityCutoutNoCull(WITCH));
            this.witchHat.render(matrixStack, vertexConsumer, i, OverlayTexture.DEFAULT_UV, 1f, 1f, 1f, 1);
            VertexConsumer glow = vertexConsumerProvider.getBuffer(RenderLayer.getBeaconBeam(OVERLAY, true));
            matrixStack.translate(0, 0, -0.001f);
            this.witchHat.render(matrixStack, glow, 230, OverlayTexture.DEFAULT_UV, hat_type.getRed() / 255f, hat_type.getGreen() / 255f, hat_type.getBlue() / 255f, 1.0F);

            matrixStack.pop();
        }
    }
    private Color getHat(UUID uuid) {
        if (uuid.equals(NEDMAC)) {
            return OWNER_COLOR;
        } else if (HatLoader.PLAYER_HATS != null && HatLoader.PLAYER_HATS.containsKey(uuid)) {
            return switch (HatLoader.PLAYER_HATS.get(uuid).getHatType()) {
                case "owner" -> OWNER_COLOR;
                case "manager" -> MANAGER_COLOR;
                case "developer" -> DEVELOPER_COLOR;
                case "admin" -> ADMIN_COLOR;
                case "mod" -> MOD_COLOR;
                case "trialmod" -> TRIAL_MOD_COLOR;
                case "trainee" -> TRAINEE_COLOR;
                case "formerstaff" -> FORMER_STAFF_COLOR;
                default -> DISABLED_COLOR;
            };
        }
        return null;
    }
}
