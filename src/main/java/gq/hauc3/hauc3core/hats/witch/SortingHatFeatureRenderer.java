package gq.hauc3.hauc3core.hats.witch;

import gq.hauc3.hauc3core.hats.web.HatLoader;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.model.TexturedModelData;
import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.feature.FeatureRenderer;
import net.minecraft.client.render.entity.feature.FeatureRendererContext;
import net.minecraft.client.render.entity.model.EntityModel;
import net.minecraft.client.render.entity.model.EntityModelLayer;
import net.minecraft.client.render.entity.model.EntityModelLoader;
import net.minecraft.client.render.entity.model.ModelWithHead;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;

import java.util.UUID;
import static gq.hauc3.hauc3core.Main.modID;

@Environment(EnvType.CLIENT)
public class SortingHatFeatureRenderer<T extends LivingEntity, M extends EntityModel<T>> extends FeatureRenderer<T, M> {
    public static final EntityModelLayer SORTING_HAT_MODEL_LAYER = new EntityModelLayer(new Identifier("hauc3core", "sorting_hat"), "main");

    private static final Identifier SORTING_HAT = new Identifier(modID, "textures/hats/sorting_hat.png");
    private final SortingHatModel<T> sortingHat;
    private final MinecraftClient client = MinecraftClient.getInstance();

    public SortingHatFeatureRenderer(FeatureRendererContext<T, M> featureRendererContext, EntityModelLoader entityModelLoader) {
        super(featureRendererContext);
        this.sortingHat = new SortingHatModel<>(entityModelLoader.getModelPart(SORTING_HAT_MODEL_LAYER));
    }

    public static TexturedModelData getTexturedModelData() {
        return TexturedModelData.of(SortingHatModel.getModelData(), 64, 128);
    }

    public void render(MatrixStack matrixStack, VertexConsumerProvider vertexConsumerProvider, int i, T livingEntity, float f, float g, float h, float j, float k, float l) {
        boolean isSortingHat = isSortingHat(livingEntity.getUuid());

        if (isSortingHat && !livingEntity.isInvisibleTo(client.player)) {
            matrixStack.push();

            ((ModelWithHead) this.getContextModel()).getHead().rotate(matrixStack);
            VertexConsumer vertexConsumer = vertexConsumerProvider.getBuffer(RenderLayer.getEntityCutoutNoCull(SORTING_HAT));
            this.sortingHat.render(matrixStack, vertexConsumer, i, OverlayTexture.DEFAULT_UV, 1f, 1f, 1f, 1);

            matrixStack.pop();
        }
    }
    private boolean isSortingHat(UUID uuid) {
        return (HatLoader.PLAYER_HATS != null && HatLoader.PLAYER_HATS.containsKey(uuid) && "sortinghat".equals(HatLoader.PLAYER_HATS.get(uuid).getHatType()));
    }
}
