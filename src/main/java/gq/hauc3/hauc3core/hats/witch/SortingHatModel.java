package gq.hauc3.hauc3core.hats.witch;

import net.minecraft.client.model.*;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.entity.model.SinglePartEntityModel;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.LivingEntity;

public class SortingHatModel<T extends LivingEntity> extends WitchHatModel<T> {
    public SortingHatModel(ModelPart root) {
        super(root);
    }
}
