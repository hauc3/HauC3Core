package gq.hauc3.hauc3core;

import gq.hauc3.hauc3core.commands.PweatherCommand;
import gq.hauc3.hauc3core.hats.web.HatLoader;
import gq.hauc3.hauc3core.hats.witch.SortingHatFeatureRenderer;
import gq.hauc3.hauc3core.hats.witch.WitchHatFeatureRenderer;
import gq.hauc3.hauc3core.util.ColorUtil;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.command.v1.ClientCommandManager;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.rendering.v1.EntityModelLayerRegistry;
import org.apache.logging.log4j.Logger;

public class Client implements ClientModInitializer {

    private static final Logger logger = Main.logger;

    @Override
    public void onInitializeClient() {
        logger.info("Initializing client...");
        EntityModelLayerRegistry.registerModelLayer(WitchHatFeatureRenderer.WITCH_HAT_MODEL_LAYER, WitchHatFeatureRenderer::getTexturedModelData);
        EntityModelLayerRegistry.registerModelLayer(SortingHatFeatureRenderer.SORTING_HAT_MODEL_LAYER, SortingHatFeatureRenderer::getTexturedModelData);
        HatLoader.init();
        ClientTickEvents.END_CLIENT_TICK.register(
                client -> ColorUtil.tick()
        );

        new PweatherCommand().register(ClientCommandManager.DISPATCHER);
    }
}
