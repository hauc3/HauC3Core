package gq.hauc3.hauc3core.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.fabricmc.fabric.api.client.command.v1.ClientCommandManager;
import net.fabricmc.fabric.api.client.command.v1.FabricClientCommandSource;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.text.LiteralText;

import java.util.function.Consumer;

public class PweatherCommand {

    public static boolean isRaining = false;
    public static float rainGradient = 0f;
    public static float thunderGradient = 0f;
    public static WeatherType pweather = WeatherType.OFF;

    public void register(CommandDispatcher<FabricClientCommandSource> dispatcher) {
        LiteralArgumentBuilder<FabricClientCommandSource> cmd = ClientCommandManager.literal("pweather");
        for (WeatherType type : WeatherType.values()) {
            cmd.then(ClientCommandManager.literal(type.name().toLowerCase()).executes(ctx -> {
                if (type != WeatherType.OFF) {
                    rainGradient = getWorld().getRainGradient(1f);
                    thunderGradient = getWorld().getThunderGradient(1f);
                    isRaining = getWorld().getLevelProperties().isRaining();
                }
                pweather = type;
                pweather.consumer.accept(getWorld());
                getPlayer().sendMessage(new LiteralText(type.msg), false);
                if (pweather != WeatherType.OFF) getPlayer().sendMessage(new LiteralText("To sync your personal weather with the server again, use /pweather off."), false);
                return 1;
            }));
        }
        dispatcher.register(cmd);
    }

    private static ClientWorld getWorld() {
        return MinecraftClient.getInstance().world;
    }

    private static ClientPlayerEntity getPlayer() {
        return MinecraftClient.getInstance().player;
    }

    public enum WeatherType {
        OFF("Your personal weather is now synced with the server again.", world -> {
            world.setRainGradient(rainGradient);
            world.setThunderGradient(thunderGradient);
            world.getLevelProperties().setRaining(isRaining);
        }),
        CLEAR("Personal weather set to clear", world -> {
            world.setRainGradient(0f);
            world.getLevelProperties().setRaining(false);
        }),
        RAIN("Personal weather set to rain", world -> {
            world.setRainGradient(1f);
            world.getLevelProperties().setRaining(true);
        }),
        THUNDER("Personal weather set to thunder", world -> {
            world.setRainGradient(1f);
            world.setThunderGradient(1f);
            world.getLevelProperties().setRaining(true);
        });

        public final String msg;
        public final Consumer<ClientWorld> consumer;

        WeatherType(String msg, Consumer<ClientWorld> consumer) {
            this.msg = msg;
            this.consumer = consumer;
        }
    }
}
