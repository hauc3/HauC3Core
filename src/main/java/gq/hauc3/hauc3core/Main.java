package gq.hauc3.hauc3core;

import net.fabricmc.api.ModInitializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main implements ModInitializer {
    public static final String modID = "hauc3core";
    public static final Logger logger = LogManager.getLogger(modID);

    @Override
    public void onInitialize() {
        logger.info("Initializing...");

//        CommandRegistrationCallback.EVENT.register((((dispatcher, dedicated) -> dispatcher.register(CommandManager.literal("foo").executes(ctx -> {
//            ctx.getSource().sendFeedback(new LiteralText("bar"), false);
//            return 1;
//        })))));
    }
}
